/*
 * Power Ampache, Ampache player for Android
 * Copyright (C) 2016  Antonio Tari
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.antoniotari.reactiveampacheapp.managers;

import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.util.Log;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.antoniotari.audiosister.AudioSister;
import com.antoniotari.reactiveampache.models.Song;

import rx.subjects.PublishSubject;
import rx.subjects.Subject;

/**
 * Created by antonio tari on 2016-05-23.
 */
public enum PlayManager {
    INSTANCE;

    private List<Song> mSongListOriginal;
    private List<Song> mSongs;
    private Song mCurrentSong;
    private boolean shuffleOn = false;
    public MediaPlayer mMediaPlayer;

    public Subject<Song, Song> mSongSubject = PublishSubject.create();

    private ArrayList<SongCompletedListener> mSongCompletedListeners;

    public interface SongCompletedListener {
        void onCompletion(MediaPlayer mediaPlayer);
    }

    public interface OnNewSongListener {
        void onNewSong(MediaPlayer mediaPlayer);
    }

    /**
     * called when the song is finished
     */
    public final OnCompletionListener completionListener = new OnCompletionListener() {
        @Override
        public void onCompletion(MediaPlayer mediaPlayer) {

            //if we are getting data there's no need to play another track
            if (mediaPlayer != null && !mediaPlayer.isPlaying()) {
                Log.d("completionListener", "getting new track");
                PlayManager.INSTANCE.mMediaPlayer = mediaPlayer;
                playNext();
            }
            fireSongCompletedListeners(mediaPlayer);
        }
    };

    public Song getNext() {
        if(mSongs==null || mSongs.isEmpty()) return null;

        if(mCurrentSong == null) {
            mCurrentSong = mSongs.get(0);
            return mCurrentSong;
        }

        int index = mSongs.indexOf(mCurrentSong);
        if (index < mSongs.size() - 1) {
            mCurrentSong = mSongs.get(index + 1);
            return mCurrentSong;
        }
        return null;
    }

    public List<Song> getCurrentPlaylist() {
        return mSongs;
    }

    public Song getPrevious() {
        if(mSongs==null || mSongs.isEmpty()) return null;
        if(mCurrentSong == null) mCurrentSong = mSongs.get(0);

        int index = mSongs.indexOf(mCurrentSong);
        if (index == 0) return mCurrentSong;
        if (index > 0) {
            mCurrentSong = mSongs.get(index - 1);
            return mCurrentSong;
        }
        return null;
    }

    public void playNext() {
        Song song = PlayManager.INSTANCE.getNext();
        if (song != null) {
            AudioSister.getInstance().playNew(song.getUrl(), song.getTitle());
        }
    }

    public void playPrevious() {
        Song song = PlayManager.INSTANCE.getPrevious();
        if (song != null) {
            AudioSister.getInstance().playNew(song.getUrl(), song.getTitle());
        }
    }

    public void setSongs(final List<Song> songs) {
        if(songs == null) return;
        mSongs = null;
        mSongs = new ArrayList<>(songs);
        if (isShuffleOn()) {
            shuffle();
        }
    }

    public void setCurrentSong(final Song currentSong) {
        mCurrentSong = currentSong;
        mSongSubject.onNext(currentSong);
    }

    public Song getCurrentSong() {
        return mCurrentSong;
    }

    public void addSongCompletedListener(final SongCompletedListener songCompletedListener) {
        if (mSongCompletedListeners == null) {
            mSongCompletedListeners = new ArrayList<>();
        }
        mSongCompletedListeners.add(songCompletedListener);
    }

    public void removeSongCompletedListener(final SongCompletedListener songCompletedListener) {
        if (mSongCompletedListeners == null || mSongCompletedListeners.isEmpty()) {
            return;
        }
        mSongCompletedListeners.remove(songCompletedListener);
    }

    public void fireSongCompletedListeners(final MediaPlayer mediaPlayer) {
        if (mSongCompletedListeners == null || mSongCompletedListeners.isEmpty()) {
            return;
        }

        for (SongCompletedListener songCompletedListener : mSongCompletedListeners) {
            songCompletedListener.onCompletion(mediaPlayer);
        }
    }

    public void playNext(final Song song) {
        if (mSongs == null) {
            mSongs = new ArrayList<>();
        }

        int posCurrent = 0;
        if (mCurrentSong != null) {
            posCurrent = mSongs.indexOf(mCurrentSong);
            mSongs.add(posCurrent + 1, song);
        } else {
            mCurrentSong = song;
            mSongs.add(song);
            playNext();
        }
    }

    public boolean shufflePlaylist() {
        if(mSongs==null) return shuffleOn;

        shuffleOn = !shuffleOn;

        if (shuffleOn) {
            shuffle();
        } else if (mSongListOriginal != null) {
            mSongs = mSongListOriginal;
            mSongListOriginal = null;
        }
//        if (mSongs != null) {
//            mCurrentSong = null;
//        }

        return shuffleOn;
    }

    private void shuffle() {
        mSongListOriginal = null;
        mSongListOriginal = new ArrayList<>(mSongs);
        Collections.shuffle(mSongs);

        // put the current song in position 0
        mSongs.remove(mCurrentSong);
        mSongs.add(0,mCurrentSong);
    }

    public boolean isShuffleOn() {
        return shuffleOn;
    }

    public void removeSong(int position) {
        if(mSongs==null) return;
        mSongs.remove(position);
    }

    /**
     * used to replace a playlist while keeping the flow of the old ones.
     * so if the current song is also in the new version of the playlist play the song
     * after instead of starting over when the current song is finished
     * @param songs
     */
    public void replacePlaylist(final List<Song> songs) {
        setSongs(songs);
        if (isSongInPlaylist(songs, mCurrentSong)) {
            mCurrentSong = checkSongInPlaylist(songs, mCurrentSong);
        }
    }

    /**
     * checks if the song is in the playlist using the song id
     *
     * @param songs
     * @param song
     * @return
     */
    public Song checkSongInPlaylist(List<Song> songs, Song song) {
        if(song == null) return null;
        if(songs == null) return null;
        if(songs.isEmpty()) return null;

        for(Song current : songs) {
            if (current!=null && current.getId().equals(song.getId())){
                return current;
            }
        }
        return null;
    }

    /**
     * checks if the song is in the playlist using the song id
     *
     * @param songs
     * @param song
     * @return
     */
    public boolean isSongInPlaylist(List<Song> songs, Song song) {
        return checkSongInPlaylist(songs, song) != null;
    }
}
