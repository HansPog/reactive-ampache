/*
 * Power Ampache, Ampache player for Android
 * Copyright (C) 2016  Antonio Tari
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
package com.antoniotari.reactiveampacheapp;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.util.List;

import com.antoniotari.audiosister.AudioSister;
import com.antoniotari.reactiveampache.api.AmpacheApi;
import com.antoniotari.reactiveampache.api.AmpacheSession;
import com.antoniotari.reactiveampache.models.Song;
import com.antoniotari.reactiveampache.utils.Log;
import com.antoniotari.reactiveampacheapp.managers.PlayManager;
import com.antoniotari.reactiveampacheapp.models.LocalPlaylist;
import com.antoniotari.reactiveampacheapp.ui.activities.BaseMusicActivity;
import com.antoniotari.reactiveampacheapp.ui.activities.SearchResultActivity;
import com.antoniotari.reactiveampacheapp.ui.activities.SplashActivity;
import com.antoniotari.reactiveampacheapp.ui.adapters.TabsAdapter;
import com.antoniotari.reactiveampacheapp.ui.adapters.TabsAdapter.TabsAdapterEntry;
import com.antoniotari.reactiveampacheapp.ui.fragments.AlbumsFragment;
import com.antoniotari.reactiveampacheapp.ui.fragments.ArtistsFragment;
import com.antoniotari.reactiveampacheapp.ui.fragments.PlaylistsFragment;

import nl.changer.audiowife.AudioWife;
import rx.android.observables.AndroidObservable;

public class MainActivity extends BaseMusicActivity {

    private Toolbar mToolbar;
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private View searchWait;

    @Override
    protected int getLayout() {
        return R.layout.activity_main;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mToolbar = (Toolbar) findViewById(R.id.toolbar);
        viewPager = (ViewPager) findViewById(R.id.viewpager);
        viewPager.setOffscreenPageLimit(3);
        tabLayout = (TabLayout) findViewById(R.id.tablayout);
        searchWait = findViewById(R.id.searchWait);

        setSupportActionBar(mToolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setHomeAsUpIndicator(R.drawable.ic_menu);
            actionBar.setDisplayHomeAsUpEnabled(false);
        }

        if (AmpacheSession.INSTANCE.isUserAuthenticated()) {
            // if the screen just rotated do not re-initialize the player
            if (AudioWife.getInstance().getMediaPlayer() == null || !AudioWife.getInstance().getMediaPlayer().isPlaying()) {
                initializePlayer();
            } else {
                // in any case re-initialize the ui
                handshakeAndInitialize();
            }
        } else {
            goToSplash();
        }
    }

    private void goToSplash() {
        startActivity(new Intent(this, SplashActivity.class));
        finish();
    }

    private void initPager() {

        TabsAdapter adapter = new TabsAdapter(getSupportFragmentManager(),
                new TabsAdapterEntry(getString(R.string.artists_title), ArtistsFragment.class),
                new TabsAdapterEntry(getString(R.string.albums_title), AlbumsFragment.class),
                new TabsAdapterEntry(getString(R.string.playlists_title), PlaylistsFragment.class)
        );
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void initializePlayer() {
        // TODO initialize with last played track
        String currentUrl = null;

        AudioSister.getInstance()
                .init(getApplicationContext(), currentUrl, mPlayBtn, mStopBtn, mSeekBar, mElapsedTimeTextView, mTotalTimeTextView,
                        MainActivity.class, R.drawable.ic_launcher, PlayManager.INSTANCE.completionListener,
                        (String playUrl, View playView) -> {

                            handshakeAndInitialize();

                            if (playUrl != null) {
                            } else {
                                AudioSister.getInstance().playCurrent();
                            }
                            AudioSister.getInstance().pause();
                        });
    }

    private void handshakeAndInitialize() {
        // handshake and then initialize
        AmpacheApi.INSTANCE.handshake()
                .subscribe(handshakeResponse -> {
                    initPager();
                    scheduleAlarm();
                }, this::onError);
    }

    public void scheduleAlarm() {
        Intent alarmIntent = new Intent(getApplicationContext(), AlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(getApplicationContext(), 0, alarmIntent, 0);

        AlarmManager manager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        int interval = 1000 * 60 * 60;
        manager.setInexactRepeating(AlarmManager.RTC, System.currentTimeMillis(), interval, pendingIntent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);

        MenuItem searchItem = menu.findItem(R.id.action_search);
        SearchView searchView;

        if (searchItem != null) {
            searchView = (SearchView) searchItem.getActionView();
            try {
                setupSearchView(searchView, searchItem);
            } catch (Exception e2) {
                Log.error(e2);
            }
        }

        return super.onCreateOptionsMenu(menu); //return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_logout) {
            AmpacheApi.INSTANCE.resetCredentials();
            goToSplash();
            return true;
        } else if (id == R.id.action_playlist) {
            // show the current playlist
            List<Song> songs = PlayManager.INSTANCE.getCurrentPlaylist();
            if (songs != null && !songs.isEmpty()) {
                goToPlaylistActivity(new LocalPlaylist(songs, getString(R.string.current_playlist)));
            } else {
                Toast.makeText(getApplicationContext(), R.string.current_playlist_empty, Toast.LENGTH_LONG).show();
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        //AudioSister.getInstance().kill();
    }

    public void setupSearchView(final SearchView searchView, final MenuItem menuItem) {
        if (menuItem == null) {
            return;
        }
        if (searchView == null) {
            return;
        }

        menuItem.setShowAsAction(MenuItem.SHOW_AS_ACTION_ALWAYS);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String input) {
                startSearch(input);
                searchView.clearFocus();
                return false;
            }

            @Override
            public boolean onQueryTextChange(String arg0) {
                return false;
            }
        });

//            searchView.setOnQueryTextFocusChangeListener(new View.OnFocusChangeListener() {
//
//                @Override
//                public void onFocusChange(View v, boolean hasFocus) {
//
//                    if (!hasFocus) {
//                        tunaActionBarMainLayout.setVisibility(View.VISIBLE);
////                                if (menuItem != null) {
////                                    menuItem.collapseActionView();
////                                }
//
//                        if (searchView != null) {
//                            searchView.setQuery("", false);
//                            ((SearchView) v).setIconified(true);
//                        }
//                    } else {
//                        tunaActionBarMainLayout.setVisibility(View.GONE);
//                    }
//                }
//            });

    }

    private void startSearch(String input) {
        searchWait.setVisibility(View.VISIBLE);
        AndroidObservable.bindActivity(MainActivity.this, AmpacheApi.INSTANCE.searchSongs(input))
                .subscribe(songs -> {
                    searchWait.setVisibility(View.GONE);
                    launchSearchResults(songs, input);
                }, error -> {
                    searchWait.setVisibility(View.GONE);
                    Toast.makeText(getApplicationContext(), R.string.search_error, Toast.LENGTH_LONG).show();
                });
    }

    private void launchSearchResults(List<Song> songs, String input) {
        if (songs == null || songs.isEmpty()) {
            Toast.makeText(getApplicationContext(), R.string.search_no_results, Toast.LENGTH_LONG).show();
        } else {
            // create a playlist with the results
            LocalPlaylist localPlaylist = new LocalPlaylist(songs, input);
            SearchResultActivity.startSearchResultActivity(this, localPlaylist);

            //searchView.setQuery("", false);
            //searchView.setIconified(true);
        }
    }
}
